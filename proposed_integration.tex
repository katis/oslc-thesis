\section{PROPOSED INTEGRATION METHODOLOGY}

FreeNEST contains several commonly used open source ALM tools and with an OSLC
API implementation it could easily connect to other vendors tools improving
FreeNEST's ease of adoption. While a full implementation for all of the
FreeNEST tools is a very large project, a great amount of value can be created
with a FreeNEST compatible implementation for a single tool.

\subsection{Service providers in FreeNEST}

On the surface, implementing the service provider architecture of FreeNEST is
fairly simple.  There is a service provider catalog in each FreeNEST instance,
which works as a "roadmap" for all of the services provided by the instance by
linking to service provider documents, each corresponding to a single tool in
the FreeNEST instance.

But the modularity of FreeNEST makes the problem more difficult. Each tool
component of a FreeNEST installation can be easily removed or installed, making
it impossible to have hard coded definitions of supported services. That is why
the OSLC implementation must be able to handle situations where only a subset
of FreeNEST components are available. Luckily the tools are installed using
Debian packages making it possible to query the operating system for the
existence of specific components.

Since the service provider architecture makes it possible to have service
provider catalogs that link to other catalogs, it is possible to have a central
server that provides an API access point to all of the FreeNEST instances in a
network. Using the catalog, a client could locate any FreeNEST in the network
and navigate to it, making it possible to create a corporate project web that
has all of the project data accessible in a common format through a single
location.

\subsection{Bugzilla as stepping stone to OSLC}

Bugzilla is a bug reporting engine used by dozens of large software projects.
It is also one of the most important development components in a FreeNEST
installation. Most of the tools in FreeNEST use a MySQL database to store their
data, and Bugzilla is no exception. That is why creating an implementation for
Bugzilla could produce code that can be reused when making OSLC
implementations for the other FreeNEST components. Bugzilla belongs to the
Change Management domain in OSLC.

\subsubsection{Bugzilla data model}

Bugzilla stores its data in a MySQL relational database and it has its own
vocabulary that differs from the OSLC Change Management specification. This
means that there needs to be a data mapping converting queries and resources to
and from the Bugzilla database. It might be easier to create an OSLC interface
that handles communication with Bugzilla using its existing XML/RPC API, but
that implementation could not be then used with any other FreeNEST tools,
because other tools do not have that API. Most FreeNEST tools use a MySQL
database and if a good implementation is made for Bugzilla, it could be used
for those other tools as well.

Figure \ref{fig:bz_changerequest} shows the Bugzilla database fields that are
needed to form a change management resource.

\hspace*{1.5cm}
\begin{figure}[htb]
\centering
\label{fig:bz_changerequest}
\includegraphics[width=\linewidth]{bz_changerequest.png}
\caption{Bugzilla database to OSLC change request conversion}
\end{figure}

The left side of Figure \ref{fig:bz_changerequest} contains Bugzilla database
fields that are needed to form the OSLC change request on the right side. The
change request is presented in RDF Turtle format. Turtle documents begin with a
list of prefix declarations, giving definitions for all of the ontologies used
in the document.

The change request resource in Figure \ref{fig:bz_changerequest} describes a
Bugzilla bug. As the figure shows, OSLC and Bugzilla use very different
vocabularies for corresponding values. Care must be taken by the service
implementors to make sure that meaning is not distorted in the conversion.

Since all resources must be identified by a URI, the service must provide a way
to have each bug resource accessible through a URL. In this integration, the
URL is formed using the identifier of a bug contained in the bug\_id column of
the bugs table in the Bugzilla database. This is just one of the ways the pure
database values have to be modified to provide a working change management
service. Similar URLs have to be provided for the users whose data is shown as
a part of the RDF document. Section 7 of Figure \ref{fig:bz_changerequest}
shows that a single field in the database can correspond to multiple values in
the document, so bug\_status must be converted to and from the various status
values contained in the RDF document.

\subsubsection{Bugzilla OSLC queries}

FreeNEST OSLC implementation should support simple queries through the OSLC
query syntax, but while OSLC queries look somewhat similar to SQL queries,
their underlying functionality is very different. SQL queries correspond to a
specific database schema, while OSLC queries are meant for retrieving data from
an RDF graph. This difference makes it fairly difficult to create an OSLC to SQL
query converter. Below is an example of an OSLC query and the MySQL query it
has to be converted to.

The following OSLC query retrieves bug resources from a tool with the specified
severity and creator:

\begin{verbatim}

nestdemo.com/bugs?oslc.where=cm:severity="high"
and dcterms:creator{foaf:accountName="john.smith@nestdemo.com"}

\end{verbatim}

It must then be converted to its corresponding MySQL query that looks something
like the following:

\begin{verbatim}

SELECT *
FROM bugs
INNER JOIN profiles AS reporter ON bugs.reporter = reporter.userid
WHERE bugs.bug_severity = "high"
AND reporter.login_name = "john.smith@nestdemo.com"

\end{verbatim}

While the queries do look a somewhat similar, there are some differences.
The most visible one between the queries is that the OSLC query does not have
the concept of tables, because it is querying an RDF graph. As such, the query
conversion does not only have to worry about converting the terminology from
OSLC to MySQL, it also has to be able to understand the differences between the
data models. After a query is converted and executed on the database, the
result set must then be transformed into an RDF document.

\section{RESULTS AND CONCLUSIONS}

\subsection{SkyNEST project implementation}

The SkyNEST project at JAMK produces the Bugzilla to OSLC implementation.  Even
though it is not (as of 28.11.2012) complete, the results are promising. The
data of each bug can be accessed through a link, queries can be made to the
service and it is possible to generate a UI preview from a link.

\subsection{Possible problems with the new integration model}

While OSLC does provide some much needed consistency to the data and user
experience in FreeNEST, implementing it is no simple task. Creating OSLC
integration even for a single tool such as Bugzilla is a major undertaking, let
alone for all of the FreeNEST components. A great deal of resources are needed
to have a full implementation for the whole platform.

Since many FreeNEST tools use MySQL databases, there is a real need for
extremely robust OSLC to SQL conversion techniques. Most of the solutions for
that area are either proprietary or insufficient for the needs of the project.
SkyNEST project has produced a converter implementation for OSLC queries that
works with Bugzilla, but only time will tell if that is a sufficient solution.

Even though Google Go is a language that is well suited to this project, it is
possible that some technical issues will arise due to its relative newness.
Go's garbage collector has some known memory leak problems on 32-bit systems,
meaning that it can fill the memory of a computer in certain cases. This bug is
being worked on by the developers, however it is not a high priority. The worst
case scenario is that FreeNEST instances would have to run only on 64-bit
machines if they wish to have the OSLC features. Java does not have this
problem and most of the OSLC community seems to use Java as their language of
choice. It is possible that Java would have been a better alternative to the
project, but most of Go's problems seem to be caused by the youth of its
implementation rather than the language itself.

\subsection{Conclusions}

OSLC specifications make it possible to link FreeNEST to the larger, more
expensive systems like IBM Jazz, removing some of the potential customer's
vendor lock-in problems. While its implementation is no easy task, it is
certainly a worthwhile project. It brings FreeNEST a major step closer to being
a well integrated open platform for software projects.

While OSLC is meant to be used with software projects, its extensibility could
make it possible to use its features and philosophy in other kinds of projects
as well.
