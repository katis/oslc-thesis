\subsection{Open Services for Lifecycle Collaboration}

Open Services for Lifecycle Collaboration community, or OSLC, was started in
2009 to define specifications to address these integration problems. The goal of
the community is to make it easier for tool developers and vendors to create
integrations between different products by creating unified technical and
conceptual specifications. OSLC community has several independent workgroups
that focus on different domains of application lifecycle.(Open Services for
Lifecycle Collaboration, 2012)

\subsubsection{OSLC Core}

OSLC Core workgroup defines the specific technical details used by the other
specialized domain workgroups.

The Core group has chosen REST and Linked Data as the main technical
implementation details. OSLC identifies resources through URLs, which can be
then modified using HTTP methods. Resources in OSLC are often data about common
concepts in ALM software, such as bugs (change requests in OSLC vocabulary).

The data in OSLC is represented with RDF, which allows software vendors to
easily add custom extensions to the data provided by their OSLC API
implementation. OSLC only defines a limited set of data that must be provided
about a specific kind of resource.

On the surface the various OSLC features may seem to be an arbitrary
combination of features, relating to the data model, vocabulary and even user
interfaces. But these features are needed to provide a general integration
interface between any tools that have an OSLC implementation.

\subsubsection{OSLC domains}

Because there are dozens of different tools in the ALM world, OSLC contains
many workgroups whose mission it is to identify common data in ALM tools that
are a part of the same problem domain. Table \ref{tab:workgroups} provides a
short description of all of the working groups in the OSLC community.

\hspace*{1.5cm}
\begin{table} [h!]
\begin{center}
\begin{tabular}{ l p{9cm} }
	
	\textbf{Workgroup name} & \textbf{Description} \\ 
	Configuration Management 	& Tries to find common definitions for managing
								software versions and configurations. \\ 
	Reporting					& Defines ways to form reports about the data
								provided by ALM tools. \\ 
	Change Management			& Focuses on tools that manage product
								change information, this often means
								bug reports. \\ 
	Quality Management			& Specifies definitions for test cases,
								test plans and test results. \\ 
	Requirements Management 	& Creates data definitions for system 
								requirements, such as what a system must do to
								fulfill its users needs and how processes
								surrounding it must be organized to make that
								system functional. \\ 
	Asset Management			& Focuses on defining asset data management.
								Assets in the software world are often binary
								files, such as dynamic linking libraries
								(DLLs), images or some kind of schematics.
								\\ 
	Architecture Management		& Defines data about project planning,
								analysis, design, construction and governance.
								\\ 
	Automation					& Specifies the resources for automation
								plans, requests and results in the software
								development cycle. \\ 
	Estimation and Measurement	& Provides specifications for estimating
								project's different aspects, such as its scope,
								cost and schedule. It also has definitions for
								monitoring, controlling and calibrating project
								estimates. \\ 
	ALM-PLM Interoperability	& Will define data for different
								interoperability scenarios with different ALM
								tools. Work in progress.  \\ 
	Performance Monitoring		& Gives specifications about different
								performance data aspects of applications. \\
								
	Reconciliation				& Specifications that will give different
								applications a way to know that they are using
								the same resource \\ 
\end{tabular}
\end{center}
	\caption{OSLC workgroups}
	\label{tab:workgroups}
\end{table}

All of the groups work independently, but the specifications produced by them
form the whole of OSLC. A full implementation of all the domains specifications
is a massive undertaking, but this thesis will focus on the implementation
details of a small subset, consisting of Core and Change Management.

\subsubsection{Service Provider architecture}

OSLC API clients can use Service Provider Catalog and Service Provider RDF
documents to discover what services an OSLC server implementations offers.
Service Provider Catalog offers links to all of the Service Providers of the
implementation and individual Service Providers list links and metadata to
different services offered by the provider. Figure \ref{fig:serviceprovider}
shows the structure of Service Provider data.

\hspace*{1.5cm}
\begin{figure}[htb]
\centering
\includegraphics[width=\linewidth]{serviceprovider.png}
\caption{Service Provider architecture}
\label{fig:serviceprovider}
\end{figure}

These documents describe all the information needed by clients to use the
supported services. OSLC services provide different types of functionality
defined by the OSLC Core specification. Here is an example of a Service
Provider document using the Turtle notation:

\begin{verbatim}

@prefix oslc: <http://open-service.net/ns/core#>.

<http://ex.com/bugz/comp1> a oslc:ServiceProvider;
    oslc:service
        [a oslc:Service;
            oslc:domain <http://open-services.net/ns/cm#>;
            oslc:queryCapability
                [a oslc:QueryCapability;
                    oslc:queryBase <http://ex.com/bugz/comp1/contents>]
            oslc:creationFactory
                [a oslc:CreationFactory;
                    oslc:creation <http://ex.com/bugz/comp1/contents>];
        ].

\end{verbatim}

The document describes a Service Provider located in
"http://example.com/bugz/comp1" which has a QueryCapability URL that can
be used for queries and a CreationFactory URL that can be used for creating new
resources. It could contain other useful metadata, such as a human readable
description of each component or technical details needed by the clients.

\subsubsection{Query Capability service}

Query capability is a way of querying OSLC resources. A resource is a network
data object or a service identified by a URI. For example, in the Change
Management domain, a resource could be a bug report for a product. Querying
these resources enables searching needed data and creating dynamic reports from
that data. OSLC does not mandate a specific query syntax, but it does provide a
definition for a query language that is relatively simple to implement. The
OSLC query syntax is meant for querying RDF data, so tools that have a
different data model, such as a relational database, need a way to convert
the OSLC queries to their own formats.

Queries can be sent with a GET HTTP request, making it possible to link to
queries. The following is an example query which retrieves a
resource with an identifier of "123" from a fictional ALM product:

\begin{verbatim}

http://alm-ex.com/oslc/cr/product/1?oslc.where=dc:identifier=123

\end{verbatim}

The part after the question mark in the query are the query
terms. Queries support searching for specific values or terms, limiting
returned data to specific values and ordering the result set.

\subsubsection{Delegated Dialog services}

Delegated dialogs are a way to provide easy integration between different OSLC
compliant tools. These dialogs can be embedded to tools with minimal code
changes and they can be used to search and create resources of a tool inside an
other tool.

A selection dialog allows users to enter search terms to find a list of
resources. These resource links can be then inserted to the parent document
that the dialog is attached to. For example, this could be used to easily link
bugs to test cases, without the need to switch between the test and bug
management tools. A basic selection dialog design can be seen in Figure
\ref{fig:selectdialog}. Selection dialogs counterpart is the creation dialog,
which allows users to create new resources inside separate tools in a uniform
way, making it easy to create bug reports and other small resources quickly.

\hspace*{1.5cm}
\begin{figure}[htb]
\centering
\includegraphics[]{selection_dialog.png}
\caption{Selection dialog sketch}
\label{fig:selectdialog}
\end{figure}

\subsubsection{Creation Factory service}

Creation factories are a way to create resources by sending RDF documents to a
address. These can be used to automate resource creation, for example creating a
bug report if a unit test fails. The factory may provide a Resource Shape. The
shape is a document that describes the information needed by the factory to
create a resource.

\subsubsection{User Interface Preview}

UI preview is a small but important feature in OSLC that make it possible for
users to see useful information about a resource with a glance.  When a user
hovers over a resource link, a small pop-up shows up at the mouse cursor,
showing a small preview of the resource, such as in Figure \ref{fig:uipreview}.

\hspace*{1.5cm}
\begin{figure}[htb]
\centering
\includegraphics[width=\linewidth]{uipreview.png}
\caption{UI Preview scetch}
\label{fig:uipreview}
\end{figure}

