\subsection{Google Go}

Go is an open source programming language originally created by Google
engineers Rob Pike, Ken Thompson and Robert Griesemer. Work on the language
began in late 2007, and the implementation was released as open source in 2009.
The language tries to solve today's software engineering problems in large scale
programs, such as those encountered by Google.

Go has some modern features, like automatic memory management with a garbage
collector, but unlike many new languages, it has strong typing and is
compiled, instead of interpreted.

\subsubsection{Google's reason for Go}

Google server software is written mostly using C++ and some Java and Python.
Google has millions of lines of code written by thousands of engineers.  C++
is a language known for its long build time with a large code base and even
with Google's massive distributed build system, compilation of a large binary 
can last as long as 27 minutes.(Go at Google, 2012)

Also, when working with the same code base with dozens, maybe hundreds of
people, having strict programming conventions becomes necessary. Most large
software development companies programming with C++ elect to use only a subset
of the language, banning the use of features deemed too complex, such as
templates and operator overloading.

\subsubsection{Improving build times with packages}

Because of the prevalence of C++ and C, it is often thought that long build
times are inevitable with compiled languages. This is not actually true.  The
use of syntactically defined packages and dependency declarations in Go makes
it possible to read exponentially less data while compiling, making build
times much shorter than in the C family of languages.

Package names are usually short and descriptive, but not unique. The package
paths differentiate between packages with the same name. Each Go source file
in the package path must begin with the package name declaration, exemplified
below:

\begin{lstlisting}
package http
\end{lstlisting}

Declaring dependencies in code is handled by the "import" statement, as shown
below. The "net/http" is a unique identifier for the package, a common
convention is that the last part of the identifier is the name of the package.

\begin{lstlisting}
import "net/http"
\end{lstlisting}

If a package with the same name exists, the following syntax can be used to
locally rename the package.

\begin{lstlisting}
import myhttp "net/http"
\end{lstlisting}

When a package is imported, its exported contents can be used with the
packages name, as shown in below. It is possible to export
package contents directly to the local scope, but this is considered to be an
extremely bad practice in normal code.

\begin{lstlisting}
// Run net/http packages inbuilt HTTP server
http.ListenAndServe(":8080", nil)
\end{lstlisting}

Because the package system describes all the dependencies in the program,
binaries can be built using a single command, no build files are necessary.
The binaries created by the Go compiler are built statically, so distribution
is extremely simple, since the user just has to download the program binary
for the right architecture, there is no need to install any code dependencies.

\subsubsection{Making teamwork easier}

Large software projects often have their own common programming conventions to
make it easier for programmers to understand their colleague's code.  These
conventions usually define the code's stylistic guidelines, such as the
placement of braces and naming of variables. They can also ban the use of some
programming language features because of the added complexity is deemed to
outweigh the benefits of those features.

The philosophy of Go is "Less Is More". In practice this means that new
features are only added to the language through careful discussion with the
language's original creators and community, who try to weigh the price of the
added complexity versus usefulness. This has the benefit that Go avoids the
feature bloat and that there is no need to select a small subset of the
language to use in a project.

Go's syntax has been designed to be easy to parse programmatically and as such
it is fairly simple to create tools for it. One such tool that comes with the
standard Go package is gofmt. The gofmt tool can automatically format Go
source code to a standard format, so that there is no need to agree any other
code format conventions besides "use gofmt on your code". Since the Go
community highly endorses the use of gofmt, most of open source code written
in Go follows a common format, making it easier to read code written by others.

Another small, but useful feature in Go, is the fact that the visibility of
functions, variables and types ("names") inside a package is decided by the
capitalization. If a name begins with a capital letter, it is visible from
outside the package, if its lower case, it can only be used inside the
package. This makes it easier to see the visibility of a name at a glance and
also gives yet another language enforced naming convention to the programmers.

\subsubsection{Reducing syntax complexity and improving safety}

Even though Go's syntax is similar to the C-family of languages, it tries to
remove some of the syntactical complexity and ambiguity of those languages.

While Go supports pointers in much the same way as C and C++, it does not
support pointer arithmetic and it is perfectly legal to take the address of a
stack variable. These features remove opportunities to practice "pointer
magic" where arbitrary memory can be read and there is also no need to think
about the situations in which you can or can not take the address of a variable.

Go also does not support using some common statements as expressions. Below are
some examples of expressions that are perfectly valid in C and C++, but not in
Go.

\begin{lstlisting}
// Assigning 2 to the index i and then incrementing i by 1
arr[i++] = 2 

// First increments i by 1 and then assigns value 2 to the incremented index i
arr[++i] = 2

// The return statement returns a which is 2
return a = 2
\end{lstlisting}

These expressions can result in code that is harder to understand and as such
Go does not allow these expressions.

\subsubsection{Lighter object orientation}

Go eschews the type of object oriented programming practiced in Java and C++.
While the language supports data with methods like Java and C++, Go does not
support inheritance. The language designers feel that inheritance causes
programmers to over engineer their early code in the fear that changing a base
class later can cause massive changes throughout the code later. Inheritance
can also create large hierarchies in problem domains that do not really have
natural hierarchies. Go does not have classes, but since any data type, even
custom integer types, can have methods structs work as replacements for
classes.

While inheritance is sometimes said to be necessary for code reuse, Go's way
of reusing code is done with interfaces and a feature called embedding.

Interfaces in Go can be combined to create larger interfaces and unlike in C++
and Java, there is no need for types to declare that they satisfy an
interface. All a type needs to do, is to implement the functions defined by the
interface, there is no "implements" syntax. This allows interface designers to
create minimal interfaces that can be later expanded if necessary. Often Go
interfaces only contain a single function. The example below shows how to
implement an interface called Reader.

\begin{lstlisting}
// This interface contains only 1 function type Reader
interface {
	Read(b []byte) (n int, e error)
}

// Implements Reader implicitly
type MyType struct {
	contents string
}

//Method declaration for MyType
func (mt *MyType) Read(bytes []byte) (readn int, error err) {
	... // method contents omitted
}
\end{lstlisting}

While Python and many other dynamic languages have similar typing system called
"duck typing" it does not exactly match the one Go uses, because dynamic
languages do all of their type checks at run-time. Go uses both compile-time
and run-time checks when doing interface typing. The way Go handles interface
typing is called structural typing.

While Go does not have true inheritance, it does support a similar feature
called embedding. In practice it means that a structure type can contain other
types, and the inner types contents become directly accessible through the
outer type. The following code example shows a way to embed an Inner type to an
Outer type, notice that while the Outer structs declaration contains the Inner
structs name, it has no variable name for it, meaning that Inner is embedded.

\begin{lstlisting}
//Inner type
type Inner struct {
	...
}

func (in Inner) PrintFoo()
{
	...
}

type Outer struct {
	Inner // Notice no variable name is set when embedding
}

// Instances of Outer can use the Inners values and methods directly:
outer := new(Outer)
outer.PrintFoo()
\end{lstlisting}

The main difference between true inheritance and embedding, is that when a
method of the inner type is called, the receiver is still the inner type.
Embedding is really just an easy to use syntax obviating the need to write
extra methods to access the functionality of the inner types.

\subsubsection{Familiar language, modern features}

While Go is reminiscent of the C-language family, it has features that are
often considered to be part of new languages. Go supports closures, meaning
anonymous functions that can refer values defined in the surrounding function.
An example of this can be seen below: 

\begin{lstlisting}
function main() {
	a := 3
	f := func() {
		fmt.Println(a)
	}
	f() // Prints "3"
}
\end{lstlisting}

Functions are also first class values in Go, they can be used as parameters to
functions and stored in variables.

\subsubsection{Easy concurrency}

Modern computers often contain more than a single CPU core. Programming
multi-core programs is difficult in many languages. Which is why it has features
that support concurrent programming. It is possible to run functions in a
lightweight process called a "goroutine". These goroutines can communicate
with each other safely and the programmer does not have to worry about setting
up memory barriers and synchronization. Using goroutines can help separating
parts of the program to independently executing components. Below is an example
of a goroutine writing the result of a computation to a channel.

\begin{lstlisting}
package main

import (
	"fmt"
	"math/rand"
)

func main() {
	// Create a channel to read the responses from.
	c := make(chan int)

	// Launches a new goroutine and
	// runs the function "foo" in it.
	go foo(c)

	// Read a value from the channel, blocks 
	// while waiting for an answer.
	res := <-c

	// Print the result
	fmt.Println(res)
}

// Return a random integer to a channel
func foo(result chan int) {
	result <- rand.Int()
}
\end{lstlisting}

Channels use the arrow operator (\textless-) to move data to and from them.
Data flows in the direction of the arrow. If a channel has no internal buffer,
like in previous code example, channel reads and writes block the goroutines.
This means that channel operations can function as communication and
synchronization points.

\subsubsection{Why use Go for the OSLC API}

FreeNEST is developed by a large group of people, most of whom are students
with varying level of programming knowledge. Some FreeNEST projects use more
common dynamic languages such as PHP and Python, but the nature of those
languages seem to cause problems when the programs start to grow in scale.
Without unit testing it is hard to be certain that a program created with a
dynamic language does not contain any serious faults, since there are no
compiler errors when giving wrong kind of data to a function. PHP and Python
with their various frameworks and libraries can also be overwhelmingly
complex, making it harder for new project members to grasp.

While Java has static typing and it is a fairly simple language, its runtime
can not be distributed with FreeNEST virtual machines due to its license.
There is an open implementation of the Java runtime called OpenJDK; however,
based on previous experience, it has problems working with several libraries.

Go has static typing, it is simple, the binaries are easy to build and
distribute, it has good tools and a fine standard library. The license is also
a permissive BSD style license. These features make it a good language to use
in a project that will have a lot of different people working on it over the
years.
